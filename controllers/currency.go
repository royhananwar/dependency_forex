package controllers

import (
	"math/rand"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetRateCurrency(c *gin.Context) {

	c.JSON(http.StatusOK, gin.H{"rate": rand.Float64()})
}
