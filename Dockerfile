FROM golang:1.18 as builder
COPY go.mod go.sum /go/src/gitlab.com/royhananwar/dependency_forex/
WORKDIR /go/src/gitlab.com/royhananwar/dependency_forex

RUN go mod download

COPY . /go/src/gitlab.com/royhananwar/dependency_forex

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/dependency_forex gitlab.com/royhananwar/dependency_forex

FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates

COPY --from=builder /go/src/gitlab.com/royhananwar/dependency_forex/build/dependency_forex /usr/bin/dependency_forex
EXPOSE 8081 8081
ENTRYPOINT ["/usr/bin/dependency_forex"]