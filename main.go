package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/royhananwar/dependency_forex/controllers"
)

func main() {

	server := gin.Default()
	server.GET("/rate", controllers.GetRateCurrency)

	server.Run(":8081")
}
